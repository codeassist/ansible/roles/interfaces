---
# file: 04.1-configure_eth.yml

- name: "04.1.1 - generate from template 'eth'-configuration(s)"
  template:
    src: "{{ ansible_os_family|lower }}.eth.j2"
    dest: "{{ interfaces_network_d_path|string }}/ifcfg-{{ item.device|string }}"
  become: True
  with_items:
    - "{{ interfaces_eth_interfaces }}"
  register: eth_interfaces_configuration

- block:
  - name: "04.1.2.1.(redhat) - generate from template route-configuration(s) (if any route on 'eth'-device is configured)"
    template:
      src: "{{ ansible_os_family|lower }}.route.j2"
      dest: "{{ interfaces_network_d_path|string }}/route-{{ item.device|string }}"
    become: True
    when:
      - item.route is defined and item.route|string|length > 0
    with_items:
      - "{{ interfaces_eth_interfaces }}"
  - name: "04.1.2.2.(redhat) - make sure route-configuration(s) was(were) removed (if route is NOT configured on specific interface)"
    file:
      path: "{{ interfaces_network_d_path|string }}/route-{{ item.device|string }}"
      state: absent
    become: True
    when:
      - (item.route is not defined) or
        (item.route is defined and item.route|string|length == 0)
    with_items:
      - "{{ interfaces_eth_interfaces }}"
  # block condition
  when:
    - ansible_os_family|lower == 'redhat'

- name: "04.1.3.1 - get facts about network devices"
  setup:
    filter: "ansible_eth[0:]"
  register: eth_facts_gathering
  changed_when:
    # if there is no variable defined in the cache ...
    - hostvars[inventory_hostname]['ansible_' ~ item.item.device] is not defined
    # ... and within the known facts
    - hostvars[inventory_hostname]['ansible_facts']['ansible_' ~ item.item.device] is not defined
  when:
    - eth_interfaces_configuration.results is defined
    - item is changed
  with_items:
    - "{{ eth_interfaces_configuration.results }}"

# Since flush_handlers doesn't support 'when' anymore, use code block below as workaround
- block:
  - name: "04.1.4.1 - stop play on localhost"
    fail:
      msg: "THIS HOST MUST BE RESTARTED MANUALLY TO CONTINUE CONFIGURING WITH ANSIBLE..."
    when:
        # stop play if it's running on local host
      - inventory_hostname == 'localhost' or
        inventory_hostname == '127.0.0.1' or
        'local' in group_names
  - name: "04.1.4.2 - trigger restart on the remote system"
    shell:
      "/bin/true"
    notify:
      - interfaces-reboot-system
      - interfaces-wait-for-coming-back
    when:
        # reboot remote host
      - inventory_hostname != 'localhost' and
        inventory_hostname != '127.0.0.1' and
        not ('local' in group_names)
  # block condition
  when:
      # only if network facts were changed ...
    - eth_facts_gathering is defined and
      eth_facts_gathering is changed
      # ... but the current environment is not Docker container
    - (ansible_virtualization_type is not defined) or
      (ansible_virtualization_type is defined and ansible_virtualization_type|lower != 'docker')

- name: "04.1.4 - make sure network devices have desired names"
  include_role:
    name: grub
  # role vars
  vars:
    grub_perform_configuration_allowed: True
    grub_restart_handler_enabled: True
    grub_config_cmdline_linux_dict:
      net.ifnames: "0"
      biosdevname: "0"
      ipv6.disable: "{{ '0' if interfaces_ipv6_enabled|bool else '1' }}"
  when:
    - eth_interfaces_configuration.results is defined and
      eth_interfaces_configuration.results|selectattr('changed', 'equalto', True)|list|length > 0
    - (ansible_virtualization_type is not defined) or
      (ansible_virtualization_type is defined and ansible_virtualization_type|lower != 'docker')

- meta: flush_handlers

- name: "04.1.5 - restart 'eth'-devices"
  shell:
    "ifdown {{ item.item.device|string }}; ip addr flush dev {{ item.item.device|string }}; ifup {{ item.item.device|string }}"
  become: True
  ignore_errors: "{{ True if (ansible_virtualization_role is defined and ansible_virtualization_role|lower != 'host' and ansible_virtualization_type is defined and ansible_virtualization_type|lower == 'docker') else False }}"
  async: 60
  poll: 3
  when:
    - eth_interfaces_configuration.results is defined
    - item is changed
    - (ansible_virtualization_type is not defined) or
      (ansible_virtualization_type is defined and ansible_virtualization_type|lower != 'docker')
  with_items:
    - "{{ eth_interfaces_configuration.results }}"
