|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/interfaces/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/interfaces/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/interfaces/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/interfaces/commits/develop)

## Ansible Role
### **_interfaces_**

An Ansible role for configuring different network interfaces:
- Ethernet interfaces
- Bridge interfaces
- Bonded interfaces
- Network routes
on RedHat/CentOS, Debian/Ubuntu and Alpine Linux servers.

_WARNING: This role can be dangerous to use. If you lose network connectivity
to your target host by incorrectly configuring your networking, you may be
unable to recover without physical access to the machine._

## Requirements

  - Ansible 2.5 and higher

## Role Default Variables
```yaml
## The list of ethernet interfaces to be added to the system
interfaces_eth_interfaces: []
### Example how to donfigure eth1 and eth2 on a host with a static IP and a dhcp IP. Also
### define static routes and a gateway:
#   interfaces_eth_interfaces:
#     - device: eth1
#       bootproto: static           # (or 'manual')
#       allow: hotplug              # (*optional)
#       family: inet                # (*optional)
#       ## IPv4 paramters if 'bootproto' = 'static'
#       address: 192.168.1.150      # (*optional)
#       netmask: 255.255.255.0      # (*optional)
#       gateway: 192.168.1.1        # (*optional)
#       network: 192.168.1.1        # (*optional)
#       broadcast: 192.168.1.255    # (*optional)
#       dnsnameservers:             # (*optional)
#         - 192.0.2.1
#         - 192.0.2.2
#       dnssearch: example.com      # (*optional)
#       mtu: 9000                   # (*optional)
#       ## This action will be added as up/post-up action to the current interface
#       route:                      # (*optional)
#         - network: 192.168.200.0
#           netmask: 255.255.255.0
#           gateway: 192.168.1.1
#           metric: 100
#         - network: 192.168.100.0
#           netmask: 255.255.255.0
#           gateway: 192.168.1.1
#       pre_up: []
#       up: []
#       down: []
#       post_down: []
#     - device: eth2
#       bootproto: dhcp
```
```yaml
## The list of bridge interfaces to be added to the system
interfaces_bridge_interfaces: []
### Example how to configure a bridge interface with multiple NICs added to the bridge. Note
### that routes can also be added for this interface in the same way routes are added for
### ethernet interfaces:
#   interfaces_bridge_interfaces:
#     - device: br1
#       address: 192.168.1.150
#       netmask: 255.255.255.0
#       bootproto: static
#       mtu: 1500
#       bridge_stp: "on"
#       bridge_ports: ['eth1', 'eth2']    # if not defined string value 'none' will be used
```
```yaml
## The list of bonded interfaces to be added to the system
interfaces_bond_interfaces: []
### Example how to configure a bond interface with an "active-backup" slave configuration:
#   interfaces_bond_interfaces:
#     - device: bond0
#       bootproto: static
#       address: 192.168.1.150
#       netmask: 255.255.255.0
#       mtu: 9000
#       bond_mode: active-backup
#       bond_miimon: 100
#       bond_slaves: [eth1, eth2]
#       route:
#         - network: 192.168.222.0
#           netmask: 255.255.255.0
#           gateway: 192.168.1.1
#
### Example how to configure a bonded interface with "802.3ad" as the bonding mode and IP
### address obtained via DHCP.
#   interfaces_bond_interfaces:
#     - device: bond0
#       bootproto: dhcp
#       bond_mode: 802.3ad
#       bond_miimon: 100
#       bond_downdelay: 200
#       bond_updelay: 200
#       bond_xmit_hash_policy: layer3+4
#       bond_slaves: [eth1, eth2]
```

## Dependencies

- interfaces + grub (to be sure interfaces has old styled system names)

## License

GPLv3

## Author Information

This role was created by Michaël Rigart <michael@netronix.be> and modified by ITSupportMe, LLC
